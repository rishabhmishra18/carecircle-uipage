// import the gulp config file
var config: MyGulp.Config = require('./gulp-config');
// NON-GULP PLUGINS
import sequence = require('run-sequence');
var del = require('del');

import gulp = require('gulp');
// GULP-LOAD-PLUGINS
import glp = require('gulp-load-plugins');
var $: MyGulp.Plugins = glp();
// BROWSER SYNC
import BrowserSync = require('browser-sync');
var bsDev = BrowserSync.create('dev');
var bsProd = BrowserSync.create('prod');
// WIREDEP
import Wiredep = require('wiredep');
var wiredep = Wiredep.stream;

// DEFAULT TASK
gulp.task('default', ['serve-dev']);
// SERVE TASKS
// dev
gulp.task('serve-dev', serveDevTask);
gulp.task('nodemon-dev', nodemonDevTask);
gulp.task('bs-dev', bsDevTask);
// build
gulp.task('serve-build', serveBuildTask);
gulp.task('nodemon-build', nodemonBuildTask);
gulp.task('bs-build', bsBuildTask);
gulp.task('build', buildTask);
// SERVE-BUILD ADDON TASKS
gulp.task('templatecache', templatecacheTask);
gulp.task('useref', userefTask);
gulp.task('rev-replace', revReplaceTask);
// WATCH TASKS
// COMPILE/TRANSPILE TASKS
gulp.task('jade-to-html', jadeToHtmlTask);
gulp.task('compile-ts', compileTsTask);
gulp.task('compile-sass', compileSassTask);
gulp.task('compile-less', compileLessTask);
// COMPILE SERVER TASKS
gulp.task('compile-server-ts', compileServerTsTask);
// COPY TASKS
gulp.task('copy-css', copyCssTask);
gulp.task('copy-images', copyImagesTask);
gulp.task('copy-fonts', copyFontsTask);
gulp.task('copy-to-tmp', copyToTmpTask);
gulp.task('copy-fonts-to-build', copyFontsToBuildTask);
gulp.task('copy-images-to-build', copyImagesToBuildTask);
// CLEANING TASKS
gulp.task('clean-srcJs-client', cleanSrcJsClientTask);
gulp.task('clean-tmp', cleanTmpTask);
gulp.task('clean-build', cleanBuildTask);
// FILE-INJECTION TASKS
gulp.task('wiredep', wiredepTask);
gulp.task('inject-js', injectJsTask);
gulp.task('inject-css', injectCssTask);
gulp.task('inject-js-tmp', injectJsTmpTask);
// WATCH TASK
gulp.task('watch-jade', watchJadeTask);
gulp.task('watch-ts', watchTsTask);
gulp.task('watch-sass', watchSassTask);
gulp.task('watch-less', watchLessTask);
gulp.task('watch-css', watchCssTask);
gulp.task('watch-images', watchImagesTask);
gulp.task('watch-fonts', watchFontsTask);
gulp.task('watch-bowerJson', watchBowerJsonTask);
gulp.task('watch-js', watchJsTask);
gulp.task('watch-indexHtml', watchIndexHtmlTask);
// NOTE - add new task to config.watchTaskArray when adding new tasks
gulp.task('watch-all', config.watchTaskArray);

// DEFAULT TASK FUNCTIONS
function defaultTask() {
    console.info('Running the default task...');
}
// SERVE TASK FUNCTIONS
// dev
function serveDevTask() {
    var taskArray = config.runSequence.dev;
    taskArray.push(taskEndHandler);

    return sequence.apply(null, taskArray);
}
function nodemonDevTask() {
    $.nodemon(config.nodemon.dev);
}
function bsDevTask() {
    bsDev.init(config.browserSync.dev);
}
// build
function serveBuildTask() {
    var buildTasks = config.runSequence.build;
    buildTasks.push(taskEndHandler);

    return sequence.apply(null, buildTasks);
}
function nodemonBuildTask() {
    console.log('Setting up the BUILD-SERVER with Nodemon');

    $.nodemon(config.nodemon.build);
}
function bsBuildTask() {
    bsProd.init(config.browserSync.build);
}
function buildTask() {
    return sequence.apply(null, config.buildTaskArray);
}
// SERVE BUILD ADDON TASK FUNCTIONS
function templatecacheTask() {
    console.info('Templatecaching html files');
    return gulp
        .src(config.filesGlob.tmp.appHtmlTemplates)
        .pipe($.angularTemplatecache(config.templatecache))
        .pipe(gulp.dest(config.dir.tmp.clientApp));
}
function userefTask() {
    console.log('Concatenating, Minifying, Mangling files, Revising files with USEREF');
    return gulp
        .src(config.files.tmp.indexHtml)
        .pipe($.useref(config.useref))
        .pipe($.if('*.js', ($.ngAnnotate())))
        .pipe($.if('*.js', $.uglify()))
        .pipe($.if('*.js', $.rev()))
        .pipe($.if('*.css', $.csso()))
        .pipe($.if('*.css', $.rev()))
        .pipe(gulp.dest(config.dir.build.root))
        .pipe($.rev.manifest())
        .pipe(gulp.dest(''));
}
function revReplaceTask() {
    console.info('REV-REPLACING js and css file versions');

    return gulp
        .src(config.files.build.indexHtml)
        .pipe($.revReplace({
            manifest: gulp.src(config.revReplace.manifest)
        }))
        .pipe(gulp.dest(config.dir.build.root));
}
// TRANSPILE TASK FUNCTIONS
function jadeToHtmlTask() {
    console.info('Compile jade into html and output to src/client/');

    return gulp
        .src(config.filesGlob.srcTs.jade)
        .pipe($.plumber(errorHandler))
        // Look only for changed files
        .pipe($.changed(config.dir.jsSrc.client, { extension: '.html' }))
        .pipe($.jade({
            pretty: true,
            doctype: 'html'
        }))
        .pipe($.plumber.stop())
        .pipe(gulp
            .dest(config.dir.jsSrc.client)
            .on('end', subTaskEndHandler));
}
function compileTsTask() {
    console.info('Transpiling TS into JS and output to client');

    return gulp
        .src(config.filesGlob.srcTs.ts)
        .pipe($.changed(config.dir.jsSrc.client, { extension: '.js' }))
        .pipe($.plumber(tsErrorHandler))
        .pipe($.typescript())
        .pipe($.plumber(errorHandler))
        .pipe(gulp
            .dest(config.dir.jsSrc.client)
            .on('end', subTaskEndHandler));
}
function compileSassTask() {
    console.info('Compiles SCSS to CSS and output to client');

    return gulp
        .src(config.files.srcTs.appScss)
        .pipe($.plumber(errorHandler))
        .pipe($.sass({ errLogToConsole: true }).on('error', $.sass.logError))
        .pipe($.autoprefixer())
        .pipe($.plumber.stop())
        .pipe(gulp.dest(config.dir.jsSrc.css))
        .pipe(bsDev.stream());
}
function compileLessTask() {
    console.info('Compiles LESS to CSS and output to client');

    var l = $.less({});
    l.on('error', function(err) {
        errorHandler(err);
        l.emit('end');
    });

    return gulp
        .src(config.files.srcTs.appLess)
        .pipe(l)
        .pipe($.autoprefixer())
        .pipe(gulp.dest(config.dir.jsSrc.css))
        .pipe(bsDev.stream());
}
// COMPILE SERVER TASK FUNCTIONS
function compileServerTsTask() {
    console.info('Compile server files and out put to src folder');

    return gulp
        .src(config.filesGlob.srcTs.server)
        .pipe($.changed(config.dir.jsSrc.server, { extension: '.js' }))
        .pipe($.plumber(errorHandler))
        .pipe($.typescript())
        .pipe(gulp.dest(config.dir.jsSrc.server));
}
// COPY TASK FUNCTIONS
function copyCssTask() {
    console.info('Copying CSS to client/styles/css folder');

    return gulp
        .src(config.filesGlob.srcTs.css)
        .pipe(gulp.dest(config.dir.jsSrc.css))
        .pipe(bsDev.stream());
}
function copyImagesTask() {
    console.info('Copying Images to client/styles folder');

    return gulp
        .src(config.filesGlob.srcTs.img)
        .pipe(gulp.dest(config.dir.jsSrc.img));
}
function copyFontsTask() {
    console.info('Copying Fonts to client/styles folder');

    return gulp
        .src(config.filesGlob.srcTs.fonts)
        .pipe(gulp.dest(config.dir.jsSrc.fonts));
}
function copyToTmpTask() {
    console.info('Copying project to .tmp/client folder');

    return gulp
        .src(config.filesGlob.js.client)
        .pipe(gulp.dest(config.dir.tmp.root));
}
function copyFontsToBuildTask() {
    console.info('Copying Fonts to build/styles/fonts folder');

    return gulp
        .src(config.filesGlob.tmp.fonts)
        .pipe($.plumber(errorHandler))
        .pipe($.plumber.stop())
        .pipe(gulp.dest(config.dir.build.fonts));
}
function copyImagesToBuildTask() {
    console.info('Copying Images to build/styles/img folder');

    return gulp
        .src(config.filesGlob.tmp.img)
        .pipe(gulp.dest(config.dir.build.img));
}
// CLEAN TASK functions
function cleanSrcJsClientTask() {
    console.info('Cleaning out the src-js/client/ folder');

    return del([
        config.dir.jsSrc.client
    ]);
}
function cleanTmpTask() {
    console.info('Cleaning out the TMP folder');
    return del([
        config.dir.tmp.root
    ]);
}
function cleanBuildTask() {
    console.info('Cleaning out the BUILD folder');
    return del([
        config.dir.build.root
    ]);
}
// FILE-INJECTION TASK FUNCTIONS
function wiredepTask() {
    console.info('WIREDEPing files into index.html');

    return gulp
        .src(config.files.srcJs.indexHtml)
        .pipe(wiredep({ ignorePath: '../..' }))
        .pipe(gulp.dest(config.dir.jsSrc.client));
}
function injectJsTask() {
    console.log('INJECTING JS files into index.html');
    return gulp
        .src(config.files.srcJs.indexHtml)
        .pipe($.inject(
            gulp.src(
                config.filesGlob.js.js,
                { read: false }),
            { ignorePath: '/src-js/client/' }
        ))
        .pipe(gulp.dest(config.dir.jsSrc.client));
}
function injectCssTask() {
    console.log('INJECTING CSS files into index.html');
    return gulp
        .src(config.files.srcJs.indexHtml)
        .pipe($.inject(
            gulp.src(config.filesGlob.js.css,
                { read: false }),
            { ignorePath: '/src-js/client' }))
        .pipe(gulp.dest(config.dir.jsSrc.client));
}
function injectJsTmpTask() {
    console.log('INJECTING JS (including templates.js) files into .tmp/index.html');

    return gulp
        .src(config.files.tmp.indexHtml)
        .pipe($.inject(gulp.src(
            config.filesGlob.tmp.js, { read: false }),
            { ignorePath: '/.tmp/client' }))
        .pipe(gulp.dest(config.dir.tmp.root));
}
// WATCH TASK FUNCTIONS
function watchJadeTask() {
    console.log('Watching Jade files');
    gulp.watch(
        config.filesGlob.srcTs.jadeWatch,
        { cwd: './' },
        ['jade-to-html']);
}
function watchTsTask() {
    console.log('Watching TS files');

    gulp.watch(
        config.filesGlob.srcTs.ts,
        ['compile-ts']);

}
function watchSassTask() {
    console.log('Watching SASS files');

    gulp.watch(
        config.filesGlob.srcTs.scss,
        ['compile-sass']);
}
function watchLessTask() {
    console.log('Watching LESS files');

    gulp.watch(
        config.filesGlob.srcTs.less,
        ['compile-less']);
}
function watchCssTask() {
    console.log('Watching CSS files');

    gulp.watch(
        config.filesGlob.srcTs.css,
        ['copy-css']);
}
function watchImagesTask() {
    console.log('Watching Image Files');

    gulp.watch(
        config.filesGlob.srcTs.img,
        ['copy-images']);
}
function watchFontsTask() {
    console.log('Watching Font Files');

    gulp.watch(
        config.filesGlob.srcTs.fonts,
        ['copy-fonts']);
}
function watchBowerJsonTask() {
    console.log('Watching bower.json for installs/uninstalls');

    gulp.watch(config.files.root.bowerJson, ['wiredep']);
}
function watchJsTask() {
    console.log('Watching JS files to inject on changes.');

    gulp.watch(
        config.filesGlob.js.js,
        ['inject-js']);
}
function watchIndexHtmlTask() {
    // specially watching index.html because any changes to index.jade will
    // trigger a new index.html which will not have the bower js/css and
    // app js/css which we inject with 'wiredep', 'inject-js' and 'inject-css'
    gulp.watch(config.files.srcJs.indexHtml, function() {
        return sequence(
            'wiredep',
            'inject-js',
            'inject-css'
        )
    });
}
// Utility functions
function errorHandler(error) {
    console.info('ERROR in gulp stream..\n', error);

    $.util.beep();
    $.util.beep();
    $.util.beep();
    return true;
}
// sepecial error handler for typescript
function tsErrorHandler(error) {
    // customize the error
    var err = {
        fileName: error.tsFile.fileName,
    };
    // log it in console with gUtil
    $.util.log(err);
}
function subTaskEndHandler() {
    $.util.beep();
    return true;
}
function taskEndHandler() {
    $.util.beep();
    $.util.beep();
    return true;
}

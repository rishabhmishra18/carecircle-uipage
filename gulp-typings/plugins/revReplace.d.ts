declare module MyGulp {
    interface Plugins extends IGulpPlugins {
        revReplace?: {
            (options?: RevReplace.Options): NodeJS.ReadWriteStream;
        }
    }
    namespace RevReplace {
        interface Options {
            canonicalUris?: boolean;
            replaceInExtensions?: Array<string>;
            prefix?: string;
            manifest?: NodeJS.ReadWriteStream;
            modifyUnreved?: Function;
            modifyReved?: Function;
        }
    }
}

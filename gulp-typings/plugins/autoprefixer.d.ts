declare module MyGulp {
    interface Plugins extends IGulpPlugins {
        autoprefixer?: {
            (opts?: AutoPrefixer.Options): NodeJS.ReadWriteStream
        }
    }
    namespace AutoPrefixer {
        interface Options {
            browsers?: string[];
            cascade?: boolean;
            remove?: boolean;
        }
    }
}

declare module MyGulp {
  interface Plugins extends IGulpPlugins {
    csso?:{
      (structureMinimization?: boolean): NodeJS.ReadWriteStream;
    }
  }
}

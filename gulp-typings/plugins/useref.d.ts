declare module MyGulp {
  interface Plugins extends IGulpPlugins {
    useref?:{
      (options?: Useref.Options, ...transformStreams: NodeJS.ReadWriteStream[]): NodeJS.ReadWriteStream;
    }
  }
  namespace Useref {
    interface Options {
        searchPath?: string | string[];
        base?: string;
        noAssets?: boolean;
        noconcat?: boolean;
        additionalStreams?: Array<NodeJS.ReadWriteStream>;
        transformPath?: (filePath: string) => void;
    }
  }
}

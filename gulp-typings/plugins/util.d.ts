declare module MyGulp {
    interface Plugins extends IGulpPlugins {
        util?: {
            beep?(): void;
            log?(err:any);
        }
    }
}

declare module MyGulp {
    interface Plugins extends IGulpPlugins {
        angularTemplatecache?: {
            (params?: GulpAngularTemplatecache.Params): any;
        }
    }
    namespace GulpAngularTemplatecache {
        interface Params {
            /*******
            * GulpAngularTemplatecache API OPTIONS
            *******/
            module?: string;
            base?: (string | { () });
            moduleSystem?: string;
            transformUrl?: { () };
            templateHeader?: string;
            templateBody?: string;
            templateFooter?: string;
            root?: string;
        }
    }
}

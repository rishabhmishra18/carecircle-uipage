declare module MyGulp {
  interface Plugins extends IGulpPlugins {
    less?:{
      (options?: Less.IOptions): NodeJS.ReadWriteStream;
    }
  }
  namespace Less {
    interface IOptions {
        modifyVars?: {};
        paths?: string[];
        plugins?: any[];
        relativeUrls?: boolean;
    }
  }
}

declare module MyGulp {
    interface Plugins extends IGulpPlugins {
        inject?: {
            (sources: NodeJS.ReadableStream, options?: Inject.IOptions): NodeJS.ReadWriteStream;
        }
    }
    namespace Inject {
        interface IOptions {
            ignorePath?: string | string[];
            relative?: boolean;
            addPrefix?: string;
            addSuffix?: string;
            addRootSlash?: boolean;
            name?: string;
            removeTags?: boolean;
            empty?: boolean;
            starttag?: string | Inject.ITagFunction;
            endtag?: string | Inject.ITagFunction;
            transform?: Inject.ITransformFunction;
            selfClosingTag?: boolean;
        }
        interface ITransformFunction {
            (filepath: string, file?: File, index?: number, length?: number, targetFile?: File): string;
        }
        interface ITagFunction {
            (targetExt: string, sourceExt: string): string;
        }
    }
}

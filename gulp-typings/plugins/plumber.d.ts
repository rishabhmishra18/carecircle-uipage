declare module MyGulp {
    interface Plugins extends IGulpPlugins {
        plumber?: {
            /**
                 * Returns Stream, that fixes pipe methods on Streams that are next in pipeline.
                 *
                 * @param options Sets options as described in the Options interface
                 */
            (options?: Plumber.Options): NodeJS.ReadWriteStream;
    		/**
    		 * Returns Stream, that fixes pipe methods on Streams that are next in pipeline.
    		 *
    		 * @param errorHandler the function to be attached to the stream on('error')
    		 */
            (errorHandler: Plumber.ErrorHandlerFunction): NodeJS.ReadWriteStream;
            /** returns default behaviour for pipeline after it was piped */
            stop(): NodeJS.ReadWriteStream;
        }
    }
    namespace Plumber {
        interface Options {
            /**
             * Handle errors in underlying streams and output them to console. Default true.
             * If function passed, it will be attached to stream on('error')
             * If false passed, error handler will not be attached
             * If undefined passed, default error handler will be attached
             */
            errorHandler?: ErrorHandlerFunction | boolean;
            /** Monkeypatch pipe functions in underlying streams in pipeline. Default true. */
            inherit?: boolean;
        }
        interface ErrorHandlerFunction {
            /** an error handler function to be attached to the stream on('error') */
            (error: any): void;
        }
    }
}

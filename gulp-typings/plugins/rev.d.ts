declare module MyGulp {
  interface Plugins extends IGulpPlugins {
    rev?:{
      (): NodeJS.ReadWriteStream;

      manifest(): NodeJS.ReadWriteStream;
      manifest(path?: string): NodeJS.ReadWriteStream;
      manifest(options?: Rev.IOptions): NodeJS.ReadWriteStream;
      manifest(path?: string, options?: Rev.IOptions): NodeJS.ReadWriteStream;
    }
  }
  namespace Rev {
    interface IOptions {
        base?: string;
        cwd?: string;
        merge?: boolean;
    }
  }
}


declare module MyGulp {

    interface Plugins extends IGulpPlugins {
        changed?: {
            (destination: string | Changed.IDestination, options?: Changed.IOptions): NodeJS.ReadWriteStream;
            compareLastModifiedTime: Changed.IComparator;
            compareSha1Digest: Changed.IComparator;
        };
    }
    namespace Changed {
        interface IDestination {
            (file: string | Buffer): string;
            compareLastModifiedTime: IComparator;
            compareSha1Digest: IComparator;
        }
        interface IComparator {
            /**
             * @param stream Should be used to queue sourceFile if it passes some comparison
             * @param callback Should be called when done
             * @param sourceFile File to operate on
             * @param destPath Destination for sourceFile as an absolute path
             */
            (stream:any, callback: Function, sourceFile: File, destPath: string): void;
        }
        interface IOptions {
            /**
             * The working directory the folder is relative to.
             * @default process.cwd()
             */
            cwd?: string;

            /**
             * Extension of the destination files.
             */
            extension?: string;

            /**
             * Function that determines whether the source file is different from the destination file.
             * @default changed.compareLastModifiedTime
             */
            hasChanged?: IComparator;
        }
    }
}

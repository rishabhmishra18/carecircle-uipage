declare module MyGulp {
    interface Plugins extends IGulpPlugins {
        sass?: {
            (opts?: Sass.Options): NodeJS.ReadWriteStream;
            logError(error?: string): void;
            sync(options?: Sass.Options): NodeJS.ReadWriteStream;
        }
    }
    namespace Sass {
        interface SassOptions {
            file?: string;
            data?: string;
            success?: (results: SassResults) => any;
            error?: (err: Error) => any;
            includePaths?: string[];
            imagePaths?: string[];
            indentedSyntax?: boolean;
            omitSourceMapUrl?: boolean;
            outFile?: string;
            outputStyle?: string;
            precision?: number;
            sourceComments?: boolean;
            sourceMap?: boolean | string;
            sourceMapEmbed?: boolean;
            sourceMapContents?: boolean;
        }

        interface Options extends SassOptions {
            errLogToConsole?: boolean;
            onSuccess?: (css: string) => any;
            onError?: (err: Error) => any;
            sync?: boolean;
        }
        interface SassResults {
            css: string;
            map: string;
            stats: {
                entry: string;
                start: Date;
                end: Date;
                duration: number;
                includedFiles: string[];
            }
        }
    }
}

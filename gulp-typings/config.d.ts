declare module MyGulp {
    interface Config {
        // files
        files: {
            srcTs: {
                appScss: string | string[];
                appLess: string | string[];
                appCss: string | string[];
            };
            srcJs: {
                indexHtml: string | string[];
            };
            tmp: {
                indexHtml: string | string[];
            };
            build: {
                indexHtml: string | string[];
            }
            root: {
                bowerJson: string | string[];
            };
        };
        // filesBlob
        filesGlob: {
            // ts
            srcTs: {
                server: string | string[];
                img: string | string[];
                fonts: string | string[];
                jade: string | string[];
                ts: string | string[];
                jadeWatch: string | string[];
                scss: string | string[];
                less: string | string[];
                css: string | string[];
            };
            js: {
                client: string | string[];
                js: string | string[];
                css: string | string[];
            };
            tmp: {
                fonts: string | string[];
                img: string | string[];
                js: string | string[];
                appHtmlTemplates: string | string[];
            };
        };
        // dir
        dir: {
            // ts
            // js
            jsSrc: {
                server: string;
                img: string;
                fonts: string;
                client: string;
                css: string;
            };
            // tmp
            tmp: {
                root: string;
                clientApp: string;
            };
            // build
            build: {
                root: string;
                fonts: string;
                img: string;
            };
        };
        // run-sequence
        runSequence: {
            dev: (string[] | (any));
            build: (string[] | (any));
        };
        // nodemon
        nodemon: {
            dev: {
                script: string;
                env: {
                    [key: string]: string | boolean | number;
                };
                ext: string;
                ignore: string[];
            };
            build: {
                script: string;
                env: {
                    [key: string]: string | boolean | number;
                };
                ext: string;
                ignore: string[];
            };
        };
        // browser-sync
        browserSync: {
            dev: {
                port: number;
                proxy: string;
            };
            build: {
                port: number;
                proxy: string;
            }
        };
        // templatecache
        templatecache: {
            module: string;
            moduleSystem: string;
            root: string;
        };
        // useref
        useref: {
            searchPath: string[];
        };
        // revReplace
        revReplace: {
            manifest: string;
        };
        // watch TASKS
        watchTaskArray: string[];
        // build Task
        buildTaskArray: string[];

    }
}

// CONFIG
/// <reference path="./config.d.ts"/>
// PLUGINS
/// <reference path="./plugins/jade.d.ts"/>
/// <reference path="./plugins/plumber.d.ts"/>
/// <reference path="./plugins/util.d.ts"/>
/// <reference path="./plugins/nodemon.d.ts"/>
/// <reference path="./plugins/changed.d.ts"/>
/// <reference path="./plugins/typescript.d.ts" />
/// <reference path="./plugins/autoprefixer.d.ts" />
/// <reference path="./plugins/sass.d.ts" />
/// <reference path="./plugins/inject.d.ts" />
/// <reference path="./plugins/angularTemplatecache.d.ts" />
/// <reference path="./plugins/useref.d.ts" />
/// <reference path="./plugins/if.d.ts" />
/// <reference path="./plugins/ngAnnotate.d.ts" />
/// <reference path="./plugins/uglify.d.ts" />
/// <reference path="./plugins/rev.d.ts" />
/// <reference path="./plugins/csso.d.ts" />
/// <reference path="./plugins/revReplace.d.ts" />
/// <reference path="./plugins/less.d.ts" />

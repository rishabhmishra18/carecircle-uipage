# typescript-jade-sass-less
A starter kit for a project that is written in Typescript, Jade, Sass &amp; Less. Written totally in `typescript`, even the `gulpfile` is in `typescript`.


## Prepare Globals

`npm install -g bower gulp browser-sync nodemon tsd typescript`

## Prepare Locals

`npm install`

`bower install`

`tsd install`


## Compile the gulp assets to run gulp commands

`npm run gulpify`

## Compile the server

`gulp compile-server-ts`

## Run the dev server

`gulp`

## Run the build server

`gulp serve-build`

### Develop in the `src` folder with `Typescript`, `jade`, `scss` & `less`

# Have Fun. Good Luck

const DEV_PORT = 5050;
const PROD_PORT = 4040;

var tsServer = 'src/server/';
var jsServer = 'src-js/server/';

var tsClient = 'src/client/';
var jsClient = 'src-js/client/';
var tmpClient = '.tmp/client/';

var build = 'build/';
// CONFIG
var config: MyGulp.Config = {
    // files
    files: {
        // tsProject
        srcTs: {
            appScss: tsClient + 'styles/sass/app-sass.scss',
            appLess: tsClient + 'styles/less/app-less.less',
            appCss: tsClient + 'styles/css/app-css.css'
        },
        // jsProject
        srcJs: {
            indexHtml: jsClient + 'index.html'
        },
        // tmp
        tmp: {
            indexHtml: tmpClient + 'index.html'
        },
        // build
        build: {
            indexHtml: 'build/index.html'
        },
        root: {
            bowerJson: 'bower.json'
        }
    },
    // filesGlob
    filesGlob: {
        // TS
        srcTs: {
            // server
            server: [
                tsServer + '**/*.ts',
                'typings/tsd.d.ts'
            ],
            // client
            img: tsClient + 'styles/img/**/*.*',
            fonts: tsClient + 'styles/fonts/**/*.*',
            jade: [
                tsClient + '**/*.jade',
                '!' + tsClient + '**/jade-includes/**/*.jade'
            ],
            ts: [
                tsClient + '**/*.ts',
                'typings/tsd.d.ts'
            ],
            jadeWatch: tsClient + '**/*.jade',
            scss: tsClient + 'styles/sass/**/*.scss',
            less: tsClient + 'styles/less/**/*.less',
            css: tsClient + 'styles/css/**/*.css'
        },
        // JS
        js: {
            client: jsClient + '**/*.*',
            js: [
                jsClient + '**/*module.js',
                jsClient + '**/*.js'
            ],
            css: jsClient + 'styles/css/**/*.css'
        },
        // tmp
        tmp: {

            fonts: tmpClient + 'styles/fonts/**/*.*',
            img: tmpClient + 'styles/img/**/*.*',
            js: [
                tmpClient + '**/*module.js',
                tmpClient + '**/*.js'
            ],
            appHtmlTemplates: tmpClient + 'app/**/*.html'
        }
    },
    // DIR
    dir: {
        // JS SRC
        jsSrc: {
            server: jsServer,
            img: jsClient + 'styles/img/',
            fonts: jsClient + 'styles/fonts/',
            client: 'src-js/client/',
            css: jsClient + 'styles/css/'
        },
        // tmp
        tmp: {
            root: tmpClient + '/',
            clientApp: tmpClient + 'app/',
        },
        // build
        build: {
            root: build,
            fonts: build + 'styles/fonts/',
            img: build + 'styles/img/'
        }
    },
    // run-sequence
    runSequence: {
        dev: [
            // CLEAN TASKS
            'clean-srcJs-client',
            // COMPILE TASKS
            'jade-to-html',
            'compile-ts',
            'compile-sass',
            'compile-less',
            // COPY TASKS
            'copy-css',
            'copy-images',
            'copy-fonts',
            // INJECT TASKS
            'wiredep',
            'inject-js',
            'inject-css',
            // SERVE TASKS
            'nodemon-dev',
            'bs-dev',
            // WATCH TASKS
            'watch-all'
        ],
        build: [
            // clean task
            'clean-srcJs-client',
            'clean-tmp',
            'clean-build',
            // COMPILE TASKS
            'jade-to-html',
            'compile-ts',
            'compile-sass',
            'compile-less',
            // COPY TASKS
            'copy-css',
            'copy-images',
            'copy-fonts',
            // INJECT TASKS
            'wiredep',
            'inject-css',
            // COPY PROJECT to .TMP folder
            'copy-to-tmp',
            // SERVE-BUILD ADDON TASKS
            'templatecache',
            // INJECT JS (which will include newly created templates.js)
            'inject-js-tmp',
            // useref
            'useref',
            // copy images and fonts to build
            'copy-fonts-to-build',
            'copy-images-to-build',
            'rev-replace',
            // SERVE TASKS
            'nodemon-build',
            'bs-build',
            // TASK END
        ]
    },
    // nodemon
    nodemon: {
        dev: {
            script: 'src-js/server/app.js',
            env: {
                PORT: DEV_PORT,
                NODE_ENV: 'development'
            },
            ext: 'html js',
            ignore: [
                '**/*.*'
            ]
        },
        build: {
            script: 'src-js/server/app.js',
            env: {
                PORT: PROD_PORT,
                NODE_ENV: 'production'
            },
            ext: 'html js',
            ignore: [
                '**/*.*'
            ]
        }
    },
    // browser-sync
    browserSync: {
        dev: {
            port: 8080,
            proxy: 'http://localhost:' + DEV_PORT,
        },
        build: {
            port: 9090,
            proxy: 'http://localhost:' + PROD_PORT,
        }
    },
    // templatecache
    templatecache: {
        module: 'app',
        moduleSystem: 'IIFE',
        root: '/app/'
    },
    useref: {
        searchPath: ['.tmp/client', '.']
    },
    revReplace: {
        manifest: 'rev-manifest.json'
    },
    // watch task
    watchTaskArray: [
        'watch-jade',
        'watch-ts',
        'watch-sass',
        'watch-less',
        'watch-css',
        'watch-images',
        'watch-fonts',
        'watch-bowerJson',
        'watch-js',
        'watch-indexHtml'
    ],
    // build task
    buildTaskArray: [
        'clean-srcJs-client',
        'clean-tmp',
        'clean-build',
        // COMPILE TASKS
        'jade-to-html',
        'compile-ts',
        'compile-sass',
        'compile-less',
        // COPY TASKS
        'copy-css',
        'copy-images',
        'copy-fonts',
        // INJECT TASKS
        'wiredep',
        'inject-css',
        // COPY PROJECT to .TMP folder
        'copy-to-tmp',
        // SERVE-BUILD ADDON TASKS
        'templatecache',
        // INJECT JS (which will include newly created templates.js)
        'inject-js-tmp',
        // useref
        'useref',
        // copy images and fonts to build
        'copy-fonts-to-build',
        'copy-images-to-build',
        'rev-replace'
    ]
};

module.exports = config;

(() => {
    'use strict';

    angular
        .module('app', ['ui.router'])
        .controller('TestCtrl', TestCtrl)
        .config(function($urlRouterProvider, $stateProvider: angular.ui.IStateProvider) {
            $urlRouterProvider.otherwise('/')

            $stateProvider
                .state('home', {
                    url: '/',
                    template: '<h1>Hello There</h1>'
                })
                .state('setting', {
                    url: '/setting',
                    templateUrl: 'app/setting.jade'
                })
                .state('test', {
                    url: '/test',
                    templateUrl: 'app/tmpls/1-tmpl.html'
                });
        });

    TestCtrl.$inject = [];

    function TestCtrl() {
        var vm = this;
        vm.heading = "[Typescript + SASS + JADE] with Gulp Automation";
        vm.greet = 'Hello!';
        vm.new = 'new';
    }
})();

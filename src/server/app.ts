import express = require('express');
import logger = require('morgan');

var app = express();

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Credentials', 'true');
    var origin = req.headers['origin'];

    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

// serve static files from root
app.use(express.static('./'));
// serve static files from "./build" or './src/client'
// depending on the environment
app.use(express.static(
    (process.env.NODE_ENV === 'production') ?
        ('./build') :
        ('./src-js/client')
));
// app.use(express.static('./build'));
// app.use('/*', express.static('./build/index.html'));
app.use('/*', express.static(
  (process.env.NODE_ENV === 'production') ?
    ('./build/index.html') :
    ('./src-js/client/index.html')
));

app
    .route('*')
    .get(function(req, res, next) {
        res.redirect('/*');
    });

process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var port = process.env.PORT || 3000;

app.listen(port, function() {
    console.log('****Starting NODE SERVER****');
    console.log('ENVIRONMENT : ' + process.env.NODE_ENV);
    console.log('Listening to port ' + port);
})

app.use(logger('dev'));
